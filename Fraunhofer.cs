﻿using System;
using System.Diagnostics;
using System.IO;

namespace FraunhoferTestaufgabe
{

    public class TaskNameSumTool
    {
        public static string[] namesList;
        public static long[] sumList;
        public static long[] multiList;

        public static String rawPath = "C:/Users/Siu/source/repos/ConsoleApp1/ConsoleApp1/bin/Debug/netcoreapp2.1/Data/names.txt";

        static void Main()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Run();
            Print();
            stopwatch.Stop();
            Console.WriteLine(namesList.Length + " items processed within " + stopwatch.ElapsedMilliseconds + " ms.");
            Console.ReadKey();
        }

        public static void Run()
        {
            namesList = GetNamesList();
            sumList = new long[namesList.Length];
            multiList = new long[namesList.Length];
            SortList(list: namesList);
            for (int i = 0; i < namesList.Length; i++)
            {
                sumList[i] = SumName(namesList[i]);
                multiList[i] = sumList[i] * (i + 1);
            }
        }

        public static string[] GetNamesList()
        {
            string path;
            if (rawPath != null) path = rawPath;
            else path = Path.Combine(Environment.CurrentDirectory, "Data", "names.txt");
            string rawData = ReadFile(path: path);
            return rawData.Substring(startIndex: 1, length: rawData.Length - 2).Split(separator: "\",\"");
        }

        public static string ReadFile(string path)
        {
            return File.ReadAllText(path: path);
        }

        public static void SortList(string[] list)
        {
            Array.Sort(array: list);
        }

        public static long SumName(string name)
        {
            long sum = 0;
            for (int i = 0; i < name.Length; i++)
                sum += CharToInteger(c: name[i]);
            return sum;
        }

        public static int CharToInteger(char c)
        {
            c = char.ToUpper(c: c);
            return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".IndexOf(value: c) + 1; //starts counting with 1 instead of 0
        }

        public static void Print()
        {
            string s = "";
            for (int i = 0; i < namesList.Length; i++)
            {
                s += namesList[i] + ": " + (i + 1) + " x " + sumList[i] + " = " + multiList[i] + "\n";
            }
            Console.WriteLine(value: s);
        }
    }
}