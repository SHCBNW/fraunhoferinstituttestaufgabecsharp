using System;
using System.IO;
using Xunit;

namespace XUnitTestProject1
{
    public class FraunhoferTest
    {
        [Fact]
        public void TestReadFile()
        {
            string actual = FraunhoferTestaufgabe.TaskNameSumTool.ReadFile(Path.Combine(Environment.CurrentDirectory, "Data", "testFile.txt"));
            Assert.Equal("AAA", actual);
        }

        [Fact]
        public void TestGetNamesListFirst()
        {
            string[] actual = FraunhoferTestaufgabe.TaskNameSumTool.GetNamesList();
            Assert.Equal("MARY", actual[0]);
        }

        [Fact]
        public void TestGetNamesListLast()
        {
            string[] actual = FraunhoferTestaufgabe.TaskNameSumTool.GetNamesList();
            Assert.Equal("ALONSO", actual[actual.Length - 1]);
        }

        [Fact]
        public void TestGetNamesListLength()
        {
            string[] actual = FraunhoferTestaufgabe.TaskNameSumTool.GetNamesList();
            Assert.True(actual.Length > 5000);

        }

        [Fact]
        public void TestSortListFirst()
        {
            string expected = "AARON";
            string[] actual = FraunhoferTestaufgabe.TaskNameSumTool.GetNamesList();
            FraunhoferTestaufgabe.TaskNameSumTool.SortList(list: actual);
            Assert.Equal(expected, actual[0]);
        }

        [Fact]
        public void TestSortListLast()
        {
            string expected = "ZULMA";
            string[] actual = FraunhoferTestaufgabe.TaskNameSumTool.GetNamesList();
            FraunhoferTestaufgabe.TaskNameSumTool.SortList(list: actual);
            Assert.Equal(expected, actual[actual.Length - 1]);
        }

        [Fact]
        public void TestSumName1()
        {
            long expected = 1;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.SumName("A");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestSumName2()
        {
            long expected = 2;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.SumName("B");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestSumName3()
        {
            long expected = 2;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.SumName("AA");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestSumName4()
        {
            long expected = 27;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.SumName("AZ");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestSumName5()
        {
            long expected = 27;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.SumName("az");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestSumName6()
        {
            long expected = 27;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.SumName("aZ");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestCharToInteger1()
        {
            int expected = 1;
            int actual = FraunhoferTestaufgabe.TaskNameSumTool.CharToInteger('a');
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestCharToInteger2()
        {
            int expected = 1;
            int actual = FraunhoferTestaufgabe.TaskNameSumTool.CharToInteger('A');
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestCharToInteger3()
        {
            int expected = 26;
            int actual = FraunhoferTestaufgabe.TaskNameSumTool.CharToInteger('Z');
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestMultiList()
        {
            FraunhoferTestaufgabe.TaskNameSumTool.Run();
            Assert.True(FraunhoferTestaufgabe.TaskNameSumTool.namesList.Length == FraunhoferTestaufgabe.TaskNameSumTool.multiList.Length);
        }

        [Fact]
        public void TestMultiList2()
        {
            // AARON = 1 x (1+1+18+15+14) = 49
            FraunhoferTestaufgabe.TaskNameSumTool.Run();
            int expected = 49;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.multiList[0];
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestMultiList3()
        {
            // "COLIN 938 x 53, also 49714"
            FraunhoferTestaufgabe.TaskNameSumTool.Run();
            int expected = 49714;
            long actual = FraunhoferTestaufgabe.TaskNameSumTool.multiList[937];
            Assert.Equal(expected, actual);
        }
    }
}
